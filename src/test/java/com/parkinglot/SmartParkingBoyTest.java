package com.parkinglot;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SmartParkingBoyTest {
    @Test
    public void should_park_first_parking_lot_when_park_given_a_smart_parking_boy_who_manage_two_parking_lot_both_with_same_number_of_empty_positions_and_a_car() {
        // given
        ParkingLot firstParkingLot = new ParkingLot(2);
        ParkingLot secondParkingLot = new ParkingLot(2);
        List<ParkingLot> parkingLots = Stream.of(firstParkingLot, secondParkingLot).collect(Collectors.toList());
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLots);
        Car car = new Car();

        // when
        ParkingTicket parkingTicket = smartParkingBoy.park(car);

        // then
        Car fetchedCar = firstParkingLot.fetch(parkingTicket);
        assertEquals(car, fetchedCar);
    }

    @Test
    public void should_park_second_parking_lot_when_park_given_a_smart_parking_boy_who_manage_two_parking_lots_that_second_one_has_more_empty_positions_and_a_car() {
        // given
        ParkingLot firstParkingLot = new ParkingLot(2);
        ParkingLot secondParkingLot = new ParkingLot(3);
        List<ParkingLot> parkingLots = Stream.of(firstParkingLot, secondParkingLot).collect(Collectors.toList());
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLots);
        Car car = new Car();

        // when
        ParkingTicket parkingTicket = smartParkingBoy.park(car);

        // then
        Car fetchedCar = secondParkingLot.fetch(parkingTicket);
        assertEquals(car, fetchedCar);
    }

    @Test
    public void should_return_right_car_when_fetch_given_a_smart_parking_boy_who_manage_two_parking_lots_both_with_a_parked_car_and_two_parking_ticket() {
        // given
        ParkingLot firstParkingLot = new ParkingLot(1);
        ParkingLot secondParkingLot = new ParkingLot(1);
        List<ParkingLot> parkingLots = Stream.of(firstParkingLot, secondParkingLot).collect(Collectors.toList());
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLots);

        Car car1 = new Car();
        Car car2 = new Car();
        ParkingTicket parkingTicket1 = smartParkingBoy.park(car1);
        ParkingTicket parkingTicket2 = smartParkingBoy.park(car2);

        // when
        Car fetchedCar1 = smartParkingBoy.fetch(parkingTicket1);
        Car fetchedCar2 = smartParkingBoy.fetch(parkingTicket2);

        // then
        assertEquals(car1, fetchedCar1);
        assertEquals(car2, fetchedCar2);
    }

    @Test
    public void should_return_unrecognized_ticket_exception_when_fetch_given_a_smart_parking_boy_who_manage_two_parking_lots_and_an_unrecognized_ticket() {
        // given
        ParkingLot firstParkingLot = new ParkingLot(0);
        ParkingLot secondParkingLot = new ParkingLot(0);
        List<ParkingLot> parkingLots = Stream.of(firstParkingLot, secondParkingLot).collect(Collectors.toList());
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLots);

        ParkingTicket unrecognizedParkingTicket = new ParkingTicket();

        // then
        Throwable exception = assertThrows(UnrecognizedTicketException.class, () -> {
            smartParkingBoy.fetch(unrecognizedParkingTicket);
        });
        assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }

    @Test
    public void should_return_unrecognized_ticket_exception_when_fetch_given_a_smart_parking_boy_who_manage_two_parking_lots_and_a_used_ticket() {
        // given
        ParkingLot firstParkingLot = new ParkingLot(1);
        ParkingLot secondParkingLot = new ParkingLot(1);
        List<ParkingLot> parkingLots = Stream.of(firstParkingLot, secondParkingLot).collect(Collectors.toList());
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLots);

        Car car1 = new Car();
        ParkingTicket ticket1 = smartParkingBoy.park(car1);
        Car fetchedCar1 = smartParkingBoy.fetch(ticket1);
        ParkingTicket usedTicket = ticket1;

        // then
        Throwable exception = assertThrows(UnrecognizedTicketException.class, () -> {
            smartParkingBoy.fetch(usedTicket);
        });
        assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }

    @Test
    public void should_return_no_available_position_exception_when_park_given_a_smart_parking_boy_who_manage_two_parking_lots_both_without_any_position_and_a_car() {
        // given
        ParkingLot firstParkingLot = new ParkingLot(1);
        firstParkingLot.park(new Car());
        ParkingLot secondParkingLot = new ParkingLot(1);
        secondParkingLot.park(new Car());
        List<ParkingLot> parkingLots = Stream.of(firstParkingLot, secondParkingLot).collect(Collectors.toList());
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLots);
        Car car = new Car();

        // then
        Throwable exception = assertThrows(NoAvailablePositionException.class, () -> {
            smartParkingBoy.park(car);
        });
        assertEquals("No available position.", exception.getMessage());
    }
}
