package com.parkinglot;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SuperParkingBoyTest {
    @Test
    public void should_park_first_parking_lot_when_park_given_given_a_super_parking_boy_who_manage_two_parking_lot_both_with_same_number_of_empty_positions_and_a_car() {
        //given
        ParkingLot firstParkingLot = new ParkingLot();
        ParkingLot secondParkingLot = new ParkingLot();
        SuperParkingBoy superParkingBoy = new SuperParkingBoy(List.of(firstParkingLot, secondParkingLot));
        Car car = new Car();

        //when
        ParkingTicket parkingTicket = superParkingBoy.park(car);

        //then
        Car fetchedCar = firstParkingLot.fetch(parkingTicket);
        assertEquals(car, fetchedCar);
    }

    @Test
    public void should_park_second_parking_lot_when_park_given_a_super_parking_boy_who_manage_two_parking_lots_that_second_one_has_more_empty_positions_and_a_car() {
        //given
        ParkingLot firstParkingLot = new ParkingLot(2);
        ParkingLot secondParkingLot = new ParkingLot(3);
        SuperParkingBoy superParkingBoy = new SuperParkingBoy(List.of(firstParkingLot, secondParkingLot));
        Car carParkedAtFirstLot = new Car();
        superParkingBoy.park(carParkedAtFirstLot);
        Car car = new Car();

        //when
        ParkingTicket ticket = superParkingBoy.park(car);

        //then
        Car fetchedCar = secondParkingLot.fetch(ticket);
        assertEquals(car, fetchedCar);
    }

    @Test
    public void should_return_right_car_when_fetch_given_a_super_parking_boy_who_manage_two_parking_lots_both_with_a_parked_car_and_two_parking_ticket() {
        //given
        ParkingLot firstParkingLot = new ParkingLot();
        ParkingLot secondParkingLot = new ParkingLot();
        SuperParkingBoy superParkingBoy = new SuperParkingBoy(List.of(firstParkingLot, secondParkingLot));
        Car car1 = new Car();
        ParkingTicket ticket1 = superParkingBoy.park(car1);
        Car car2 = new Car();
        ParkingTicket ticket2 = superParkingBoy.park(car2);

        //when
        Car ParkedCar1 = superParkingBoy.fetch(ticket1);
        Car ParkedCar2 = superParkingBoy.fetch(ticket2);

        //then
        assertEquals(car1, ParkedCar1);
        assertEquals(car2, ParkedCar2);
    }

    @Test
    void should_return_unrecognized_ticket_exception_when_fetch_given_a_super_parking_boy_who_manage_two_parking_lots_and_an_unrecognized_ticket() {
        //given
        ParkingLot firstParkingLot = new ParkingLot();
        ParkingLot secondParkingLot = new ParkingLot();
        SuperParkingBoy superParkingBoy = new SuperParkingBoy(List.of(firstParkingLot, secondParkingLot));
        Car car = new Car();
        superParkingBoy.park(car);
        ParkingTicket ticket = new ParkingTicket();

        //then
        String exception = assertThrows(UnrecognizedTicketException.class, () -> {
            superParkingBoy.fetch(ticket);
        }).getMessage();
        assertEquals("Unrecognized parking ticket.", exception);
    }

    @Test
    void should_return_unrecognized_ticket_exception_when_fetch_given_a_super_parking_boy_who_manage_two_parking_lots_and_a_used_ticket() {
        //given
        ParkingLot firstParkingLot = new ParkingLot();
        ParkingLot secondParkingLot = new ParkingLot();
        SuperParkingBoy superParkingBoy = new SuperParkingBoy(List.of(firstParkingLot, secondParkingLot));
        Car car1 = new Car();
        ParkingTicket ticket1 = superParkingBoy.park(car1);
        Car fetchedCar1 = superParkingBoy.fetch(ticket1);
        ParkingTicket usedTicket = ticket1;

        //then
        String exception = assertThrows(UnrecognizedTicketException.class, () -> {
            superParkingBoy.fetch(usedTicket);
        }).getMessage();
        assertEquals("Unrecognized parking ticket.", exception);
    }

    @Test
    void should_return_no_available_position_exception_when_park_given_a_super_parking_boy_who_manage_two_parking_lots_both_without_any_position_and_a_car() {
        //given
        ParkingLot firstParkingLot = new ParkingLot(1);
        firstParkingLot.park(new Car());
        ParkingLot secondParkingLot = new ParkingLot(1);
        secondParkingLot.park(new Car());
        SuperParkingBoy superParkingBoy = new SuperParkingBoy(List.of(firstParkingLot, secondParkingLot));
        Car car = new Car();

        //then
        String exceptionMessage = assertThrows(NoAvailablePositionException.class, () -> {
            superParkingBoy.park(car);
        }).getMessage();
        assertEquals("No available position.", exceptionMessage);
    }
}

