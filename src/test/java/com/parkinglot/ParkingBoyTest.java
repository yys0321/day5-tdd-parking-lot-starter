package com.parkinglot;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

public class ParkingBoyTest {
    @Test
    public void should_park_first_parking_lot_when_park_given_a_parking_boy_who_manage_two_parking_lots_both_with_available_position_and_a_car() {
        // given
        ParkingLot firstParkingLot = new ParkingLot(2);
        ParkingLot secondParkingLot = new ParkingLot(2);
        List<ParkingLot> parkingLots = Stream.of(firstParkingLot, secondParkingLot).collect(Collectors.toList());
        ParkingBoy standardParkingBoy = new ParkingBoy(parkingLots);
        Car car = new Car();

        // when
        ParkingTicket parkingTicket = standardParkingBoy.park(car);

        // then
        Car fetchedCar = firstParkingLot.fetch(parkingTicket);
        assertEquals(car, fetchedCar);
    }

    @Test
    public void should_park_second_parking_lot_when_park_given_a_parking_boy_who_manage_two_parking_lot_that_first_is_full_and_second_with_available_position_and_a_car() {
        // given
        ParkingLot firstParkingLot = new ParkingLot(0);
        ParkingLot secondParkingLot = new ParkingLot(2);
        List<ParkingLot> parkingLots = Stream.of(firstParkingLot, secondParkingLot).collect(Collectors.toList());
        ParkingBoy standardParkingBoy = new ParkingBoy(parkingLots);
        Car car = new Car();

        // when
        ParkingTicket parkingTicket = standardParkingBoy.park(car);

        // then
        Car fetchedCar = secondParkingLot.fetch(parkingTicket);
        assertEquals(car, fetchedCar);
    }

    @Test
    public void should_return_right_car_when_fetch_given_a_parking_boy_who_manage_two_parking_lots_both_with_a_parked_car_and_two_parking_tickets() {
        // given
        ParkingLot firstParkingLot = new ParkingLot(1);
        ParkingLot secondParkingLot = new ParkingLot(1);
        List<ParkingLot> parkingLots = Stream.of(firstParkingLot, secondParkingLot).collect(Collectors.toList());
        ParkingBoy standardParkingBoy = new ParkingBoy(parkingLots);
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingTicket ticket1 = standardParkingBoy.park(car1);
        ParkingTicket ticket2 = standardParkingBoy.park(car2);

        // when
        Car fetchedCar1 = standardParkingBoy.fetch(ticket1);
        Car fetchedCar2 = standardParkingBoy.fetch(ticket2);

        // then
        assertEquals(car1, fetchedCar1);
        assertEquals(car2, fetchedCar2);
    }

    @Test
    public void should_return_unrecognized_ticket_exception_when_fetch_given_a_parking_boy_who_manage_two_parking_lots_and_an_unrecognized_ticket() {
        // given
        ParkingLot firstParkingLot = new ParkingLot(2);
        ParkingLot secondParkingLot = new ParkingLot(2);
        List<ParkingLot> parkingLots = Stream.of(firstParkingLot, secondParkingLot).collect(Collectors.toList());
        ParkingBoy standardParkingBoy = new ParkingBoy(parkingLots);
        ParkingTicket UnrecognizedTicket = new ParkingTicket();

        // then
        Throwable exception = assertThrows(UnrecognizedTicketException.class, () -> {
            standardParkingBoy.fetch(UnrecognizedTicket);
        });
        assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }

    @Test
    public void should_return_unrecognized_ticket_exception_when_fetch_given_a_parking_boy_who_manage_two_parking_lots_and_a_used_ticket() {
        // given
        ParkingLot firstParkingLot = new ParkingLot(1);
        ParkingLot secondParkingLot = new ParkingLot(1);
        List<ParkingLot> parkingLots = Stream.of(firstParkingLot, secondParkingLot).collect(Collectors.toList());
        ParkingBoy standardParkingBoy = new ParkingBoy(parkingLots);
        Car car1 = new Car();
        ParkingTicket ticket1 = standardParkingBoy.park(car1);
        Car fetchedCar1 = standardParkingBoy.fetch(ticket1);
        ParkingTicket usedTicket = ticket1;

        // then
        Throwable exception = assertThrows(UnrecognizedTicketException.class, () -> {
            standardParkingBoy.fetch(usedTicket);
        });
        assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }

    @Test
    public void should_return_no_available_position_exception_when_park_given_a_parking_boy_who_manage_two_parking_lots_both_without_any_position_and_a_car() {
        // given
        ParkingLot firstParkingLot = new ParkingLot(1);
        firstParkingLot.park(new Car());
        ParkingLot secondParkingLot = new ParkingLot(1);
        secondParkingLot.park(new Car());
        List<ParkingLot> parkingLots = Stream.of(firstParkingLot, secondParkingLot).collect(Collectors.toList());
        ParkingBoy standardParkingBoy = new ParkingBoy(parkingLots);
        Car car = new Car();

        // then
        Throwable exception = assertThrows(NoAvailablePositionException.class, () -> {
            standardParkingBoy.park(car);
        });
        assertEquals("No available position.", exception.getMessage());
    }
}

