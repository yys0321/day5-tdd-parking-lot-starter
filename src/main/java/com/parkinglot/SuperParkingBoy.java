package com.parkinglot;

import java.util.Comparator;
import java.util.List;

public class SuperParkingBoy extends ParkingBoy {
    public SuperParkingBoy(List<ParkingLot> parkingLots) {
        super(parkingLots);
    }

    public ParkingTicket park(Car car) {
        return parkingLots.stream()
                .filter(parkingLot -> parkingLot.getCapacity() != 0)
                .max(Comparator.comparing(parkingLot -> (double) parkingLot.getRemainingCapacity() / parkingLot.getCapacity()))
                .orElseThrow(() -> new NoAvailablePositionException("No available position."))
                .park(car);
    }

    public Car fetch(ParkingTicket ticket) {
        return parkingLots.stream()
                .filter(parkingLot -> parkingLot.isAssociateWithTicket(ticket))
                .findFirst()
                .orElseThrow(() -> new UnrecognizedTicketException("Unrecognized parking ticket."))
                .fetch(ticket);
    }
}
