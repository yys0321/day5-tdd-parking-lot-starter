package com.parkinglot;

import java.util.HashMap;

public class ParkingLot {
    private static final int DEFAULT_CAPACITY = 10;
    private final int capacity;
    HashMap<ParkingTicket, Car> parkedPosition = new HashMap<>();

    public ParkingLot(int capacity) {
        this.capacity = capacity;
    }

    public ParkingLot() {
        this(DEFAULT_CAPACITY);
    }

    public ParkingTicket park(Car car) {
        if (parkedPosition.size() == capacity) {
            throw new NoAvailablePositionException("No available position.");
        }
        ParkingTicket ticket = new ParkingTicket();
        parkedPosition.put(ticket, car);
        return ticket;
    }

    public Car fetch(ParkingTicket ticket) {
        if (!parkedPosition.containsKey(ticket)) {
            throw new UnrecognizedTicketException("Unrecognized parking ticket.");
        }
        return parkedPosition.remove(ticket);
    }

    public boolean isFull() {
        return parkedPosition.size() == capacity;
    }

    public boolean isAssociateWithTicket(ParkingTicket ticket) {
        return parkedPosition.containsKey(ticket);
    }

    public int getRemainingCapacity() {
        return capacity - parkedPosition.size();
    }

    public int getCapacity() {
        return capacity;
    }
}
