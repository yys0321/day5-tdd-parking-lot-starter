package com.parkinglot;

import java.util.Comparator;
import java.util.List;

public class SmartParkingBoy extends ParkingBoy {
    public SmartParkingBoy(List<ParkingLot> parkingLots) {
        super(parkingLots);
    }
    
    public ParkingTicket park(Car car) {
        return parkingLots.stream()
                .filter(parkingLot -> !parkingLot.isFull())
                .max(Comparator.comparingInt(ParkingLot::getRemainingCapacity))
                .orElseThrow(() -> new NoAvailablePositionException("No available position."))
                .park(car);
    }

    public Car fetch(ParkingTicket ticket) {
        return parkingLots.stream()
                .filter(parkingLot -> parkingLot.isAssociateWithTicket(ticket))
                .findFirst()
                .orElseThrow(() -> new UnrecognizedTicketException("Unrecognized parking ticket."))
                .fetch(ticket);
    }
}
