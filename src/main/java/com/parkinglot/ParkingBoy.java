package com.parkinglot;

import java.util.List;

public class ParkingBoy {
    private ParkingLot parkingLot;
    public List<ParkingLot> parkingLots;

    public ParkingBoy(ParkingLot parkingLot) {
        this.parkingLot = parkingLot;
        this.parkingLots = null;
    }

    public ParkingBoy(List<ParkingLot> parkingLots) {
        this.parkingLots = parkingLots;
        this.parkingLot = null;
    }

    public ParkingTicket park(Car car) {
        if (this.parkingLot != null) {
            return this.parkingLot.park(car);
        } else {
            return parkingLots.stream()
                    .filter(parkingLot -> !parkingLot.isFull())
                    .findFirst()
                    .orElseThrow(() -> new NoAvailablePositionException("No available position."))
                    .park(car);
        }
    }

    public Car fetch(ParkingTicket ticket) {
        if (this.parkingLot != null) {
            return this.parkingLot.fetch(ticket);
        } else {
            return parkingLots.stream()
                    .filter(parkingLot -> parkingLot.isAssociateWithTicket(ticket))
                    .findFirst()
                    .orElseThrow(() -> new UnrecognizedTicketException("Unrecognized parking ticket."))
                    .fetch(ticket);
        }
    }
}

